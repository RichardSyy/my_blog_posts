# Internship Experience at Musicfox

I interned with __Musicfox__ during the summer of 2019, from June 17th 
all the way to September 6th, 2019.  

During the internship, I focus mainly on data engineering and also 
on feature development. More specifically, I'm in charge of supplying 
data to our data scientist and at times of presenting some of the 
findings by developing interactive data visualization applications. 
I accomplish these tasks purely in Python, with the help of 
packages like [`pandas`](https://pandas.pydata.org/), 
[`json`](https://docs.python.org/3.7/library/json.html) 
and [`plotly/dash`](https://plot.ly/dash/).

This has been an exceptional experience for me and I've learned a ton 
in the process. As such, it makes sense to document and share what 
I've gained in the form of a summary.  

## Summary of What I've Learned  

1. The Git + Bash workflow  
2. Test-driven development by making use of `pytest`  
3. Interactive web application development with `plotly/dash`  
4. Data acquisition and cleaning using `pandas` and `json`  
5. Prepare the `pycm` library for open-sourcing  
6. Tech-documentation composition related to Python and Bash  

In the sections to follow, I will describe and share my experience in 
more detail (to the extent allowed in the NDA that is) for each of the 
bullet-points listed above.

## `Git` + `Bash` Workflow

Prior to my internship here at __Musicfox__, I didn't know much, if 
anything, about either Git or Bash. I've only been able to manually 
click and download files from others' repos on the GitLab or GitHub. 
Not that I hadn't tried to make use of the Git by command lines, but 
my previous lack-of-experience with Command-line Interface languages 
in general prevented me from successfully doing that. Needless to 
say, I was also not too competent in Bash, which is a command-line 
language, at all. 

Once we (another intern from my program and I) jumped onboard, 
[Jason](https://gitlab.com/thinkjrs) gave us the tour and introduced 
us to the Git and Bash workflow, which I shall briefly do the same 
in this section here.

So to start, the Git is essential for version-control and 
collaborating with others on projects, especially larger and more 
sophisticated ones. The Bash, on the other hand, is a Unix shell and 
command-line language that's useful for navigating through the 
directories and manipulating files without the file explorer, while 
capable of doing many more advanced things.  

To have a taste of what they are capable of, here are the most 
extensively used and probably the most useful Git and Bash commands 
based on my experience throughout the summer:  
* Clone from an existing repo by SSH
```bash
$ git clone git@gitlab.com:user_name/my_project.git 
```
* Pull files from the remote repo so that it's up-to-date
```bash
$ git pull 
```
* Create a new branch off current branch
```bash
$ git branch myBranch 
```
* Migrate to myBranch
```bash
$ git checkout myBranch 
```
* Check for files untracked, modified, waiting to be committed
```bash
$ git status 
```
* Add myFile.txt to the staging area
```bash
$ git add myFile.txt 
```
* Commit the staged change to local branch
```bash
$ git commit -m "this is my commit message"
```
* Push the committed change to remote branch
```bash
$ git push 
```
* Create new directory myDir
```bash
$ mkdir myDir
```
* Switch current directory to myDir
```bash
$ cd myDirectory 
```
* Remove all files and directories within trashDir
```bash
$ rm -rf trashDir
```
* Rename myFile.txt as newFile.txt
```bash
$ mv myFile.txt newFile.txt
```
* Copy all files and directories within oldDir to newDir
```bash
$ cp -r oldDir/. newDir/.
```
* List all files and directories, including hidden ones, with detail
```bash
$ ls -la
```

Other than the command-line part of the workflow, filing issue reports 
and submitting merge requests properly is something I've learned and 
exercised extensively during the summer internship that is also of 
great importance. It is essential for documenting and communicating 
with other team members so that necessary work gets done and no time 
is wasted on working on something that has already been done.  

## Test-driven Development with `pytest`

A test-driven development workflow is one where you would write out 
the test cases first, and then build up program or feature aiming to 
pass the tests. By defining the expected behavior of the program 
beforehand, it's easier to keep track of the goals and create code 
that is less prone to bugs or unexpected behaviors. 

For the summer, I've taken up the specific approach of starting with 
writing out the function calls and defining tests that check if the 
function does return something and that the returned object is of the 
desired data type or if the result is correct when the function is 
doing something very simple. After the function has been mostly coded 
up, I'd run the function call test I've built up earlier to make sure 
they pass. I would then add a few more tests depending on the actual 
function, to thoroughly check if the function is performing as desired.  

There are a few ways to write tests in Python. One way is to use 
the [`unittest`](https://docs.python.org/3.7/library/unittest.html) 
package as we've been introduced in the _Computing for Finance in 
Python_ course taught by 
[Sebastien](https://finmath.uchicago.edu/about/faculty-and-lecturers/sebastien-donadio/). 
During the internship here at __Musicfox__, we were introduced to use 
the [`pytest`](https://docs.pytest.org/en/latest/) library to write 
test cases. 

The reason for favoring `pytest` over `unittest` is that writing tests 
is easier because you can just write test functions and don't have to 
create an entire test class as you would with `unittest`. In addition, 
the `pytest.fixture` object is a convenient and powerful tool for 
preparing resources used in the tests. You define a function that 
prepares what you need, let it be data input or desired results, and 
wrap it into the `@pytest.fixture` decorator. Then the function you 
defined can be simply passed as arguments into the the test functions 
for use. And of course, it provides very clear stack trace and error 
information for debugging. 

To use the testing feature, the name of the test functions must start 
with "test_". Otherwise `pytest` cannot figure out which functions to 
test. However, it is not required to have dedicated Python files to 
store the test functions, but it is recommended to stay organized and 
have the name of the Python file start with "test_" as well. Here's a 
very simple example where I have both the function and the 
corresponding tests in the same file called `add_and_test.py`.  
```python 
import pytest

@pytest.fixture
def cases(): 
    # simply return the dict with test cases and correct results
    return { 
        (1, 2): 3,
        (5, 2): 7,
        (8, 9): 17,
        (13, 93): 106,
        (7832, 8012): 15844,
    }

def test_add(cases): 
    for case, result in cases.items():
        add_result = add(*case)
        assert result == add_result

def add(num1, num2):
    return num1 + num2
```

To run the test of the function `add` using the file as specified 
above, simply type `pytest -vv add_and_test.py` in the command-line 
console and it will spit out the test result in a few moments if 
everything is correctly set up. The file name `add_and_test.py` here 
can be omitted if you wish to check for eligible tests in all files 
within the current directory. If, on the other hand, you have multiple 
test functions and you'd like to test only a few of them, add a `-k` 
option followed by a specific keyword for selection of tests.  

## Interactive Web App with `plotly/dash`

Before entering the internship, my only experience with front-end development was from an undergraduate course on web-page design, which was almost entirely taught without coding using Adobe Dreamweaver with occasional reference of HTML in the process.  

Here during the internship at __Musicfox__, I was introduced to the ['plotly/dash'](https://plot.ly/dash/) library, with which one can "build beautiful web-based analytic apps" and "no JavaScript required". This makes creating data visualization apps and features very easy for data scientists or financial engineers or generally any pure Python users with little experience on front-end application development or JavaScript.  

During my internship, the relating work I've done mostly involve the development of features that would be plugged into a greater framework of our Musicfox web application. The first one I've built is a 3D graph that displays artist ranks in charts with the mean and volatility of changes in number of streams for a given set of artists. Following this one, I then worked on putting together the box chart of the number of streams for different types of tracks, categorized based on the record label and the number of composers and collaborators. Other than the two, I was also tasked with plotting an approximated Mean-Variance frontier for the universe of artists. The features I've created are in no shape or form mind-blowing, but the fact that I've got to know it's possible to visualize data and create interactive apps and features in pure Python let alone actually gained first-hand experience in the process is massively valuable to me.



We started out by going through the [Dash App Gallery](https://dash-gallery.plotly.host/Portal/), where a bunch of interactive data visualization examples built using `plotly/dash` are being displayed, to get a sense of what the toolset is capable of.  

After going through some basic tutorials, I was tasked to 


tasked with building the feature displaying the 3D graph of 


## Data Acquisition with `pandas` and `json`



## Tech-documentation Composition



## Adding proper docstring and comments to a library for open-sourcing


